import java.time.Instant
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

plugins {
    kotlin("jvm") version "1.9.23"
    id("com.google.cloud.tools.jib") version "3.4.2"
}

val group = "com.pillpall"
val version = "0.1.0-ALPHA.1-SNAPSHOT"
val tag = generateTag(version)

repositories {
    mavenCentral()
}

dependencies {
    testImplementation("org.jetbrains.kotlin:kotlin-test")
    implementation("org.tinylog:slf4j-tinylog:2.7.0")
    implementation("org.tinylog:tinylog-impl:2.7.0")
}

tasks.test {
    useJUnitPlatform()
}

kotlin {
    jvmToolchain(21)
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "21"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "21"
    }
    wrapper {
        gradleVersion = "8.7"
        distributionType = Wrapper.DistributionType.BIN
    }
}

jib {
    from {
        image =
            "bellsoft/liberica-runtime-container:jre-21-musl@sha256:f8cf84cbb33ca8dfcfed323c1b5ade51ed842da78ec8c3d75942b3ada9f5d6d2"
    }

    to {
        image = "registry.gitlab.com/wendersonferreira/${project.name}"
        tags = setOf(tag)

        auth {
            username = System.getenv("REGISTRY_USER")
            password = System.getenv("REGISTRY_KEY")
        }
    }

    container {
        labels.set(
            mapOf(
                "maintainer" to "Wenderson Ferreira de Souza <wenderson@trustsystems.com.br>",
                "org.opencontainers.image.title" to project.name,
                "org.opencontainers.image.description" to "Pillpal",
                "org.opencontainers.image.version" to tag,
                "org.opencontainers.image.authors" to "Wenderson Ferreira de Souza <wenderson@trustsystems.com.br>",
                "org.opencontainers.image.url" to "https://gitlab.com/wendersonferreira/${project.name}",
                "org.opencontainers.image.vendor" to "https://gitlab.com/wendersonferreira/${project.name}",
                "org.opencontainers.image.licenses" to "TSL"
            )
        )
        creationTime = Instant.now().toString()
        jvmFlags = listOf(
            "-server",
            "-XX:+UseContainerSupport"
        )
        mainClass = "com.pillpal.ApplicationEntryPointKt"
        volumes = listOf("/data")
    }
}

fun generateTag(version: String): String {
    return when (version.substringAfterLast("-")) {
        "SNAPSHOT" ->
            version.replace("-SNAPSHOT", "")
                .plus("_").plus(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")))

        else -> version
    }
}